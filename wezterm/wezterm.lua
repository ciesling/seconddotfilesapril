local wezterm = require 'wezterm'
local config = {}

-- config.font = wezterm.font 'GeistMono Nerd Font'
-- config.font = wezterm.font 'Ubuntu Mono Ligaturized'
config.font = wezterm.font_with_fallback({
    { family = 'Operator Mono Lig', scale = 1.0 },
    { family = 'FiraCode Nerd Font Mono', scale = 1.0 },
    { family = 'GeistMono Nerd Font', scale = 1.0 },
    { family = 'Ubuntu Mono Ligaturized', scale = 1.0 },
})
config.color_scheme = 'Snazzy (base16)'
config.adjust_window_size_when_changing_font_size = false

config.animation_fps = 60
config.cursor_blink_ease_in = 'Linear'
config.cursor_blink_ease_out = 'Linear'
config.cursor_blink_rate = 1100
config.default_cursor_style = 'BlinkingUnderline'
config.cursor_thickness = '0.05cell'

config.automatically_reload_config = true
config.hide_tab_bar_if_only_one_tab = true
config.audible_bell = 'Disabled'
config.window_close_confirmation = 'NeverPrompt'

return config
